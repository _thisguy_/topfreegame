var app = angular.module("game-app", []);

app.controller("featuredGames", function($scope){
	$(document).ready(function(){
		$(".featured-games h4").css('padding-left', $(".featured-games .featured-game").css('padding-left'));
	});
	
	var _featuredGamesList = [
		{
			id: 1,
			name: "Game1",
			link: "#1",
			image: "top-game1.jpg",
			description: "Winter is coming, so get comfy and relax with a cute Christmas version of the timeless card classic during the holiday season! Objective of this Solitaire Klondike game is to move all cards onto the four foundation piles, sorted by suit and rank in ascending order from Ace to King. On the field, cards can only be sorted in descending order alternating colors. Can you earn a high score?"
		},
		{
			id: 2,
			name: "Game2",
			link: "#2",
			image: "top-game2.jpg",
			description: "This is game2 description"
		}
	];
	$scope.featuredGamesList = _featuredGamesList;
});

app.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);
    for (var i=0; i<total; i++)
      input.push(i);
    return input;
  };
});

app.controller('top5', function($scope){
	var _categories = [
	{
		name: 'category1',
		games:[
			{
				id: 1,
				name: "Game1",
				link: "google.com",
				image: "top-game1.jpg",
				rating: 5
			},
			{
				id: 2,
				name: "Game2",
				link: "#2",
				image: "sample-1_alternative.jpg",
				rating: 4
			},
			{
				id: 3,
				name: "Game3",
				link: "#3",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 4,
				name: "Game4",
				link: "#4",
				image: "top-game1.jpg",
				rating: 4
			},
			{
				id: 5,
				name: "Game5",
				link: "#5",
				image: "logo.png",
				rating: 4
			}
		]
	}, 
	{
		name: 'category2',
		games:[
			{
				id: 1,
				name: "Game1",
				link: "#1",
				image: "top-game1.jpg",
				rating: 5
			},
			{
				id: 2,
				name: "Game2",
				link: "#2",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 3,
				name: "Game3",
				link: "#3",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 4,
				name: "Game4",
				link: "#4",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 5,
				name: "Game5",
				link: "#5",
				image: "top-game2.jpg",
				rating: 4
			}
		]
	},
	{
		name: 'category3',
		games:[
			{
				id: 1,
				name: "Game1",
				link: "#1",
				image: "top-game1.jpg",
				rating: 5
			},
			{
				id: 2,
				name: "Game2",
				link: "#2",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 3,
				name: "Game3",
				link: "#3",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 4,
				name: "Game4",
				link: "#4",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 5,
				name: "Game5",
				link: "#5",
				image: "top-game2.jpg",
				rating: 4
			}
		]
	},
	{
		name: 'category4',
		games:[
			{
				id: 1,
				name: "Game1",
				link: "#1",
				image: "top-game1.jpg",
				rating: 5
			},
			{
				id: 2,
				name: "Game2",
				link: "#2",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 3,
				name: "Game3",
				link: "#3",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 4,
				name: "Game4",
				link: "#4",
				image: "top-game2.jpg",
				rating: 4
			},
			{
				id: 5,
				name: "Game5",
				link: "#5",
				image: "top-game2.jpg",
				rating: 4
			}
		]
	}
	];
	$scope.categories = _categories;

	$scope.directing = function directing (link) {
		window.location.href = link;
	}
});

app.directive('navigation', function(){
	return {
		restrict: 'E', 
		templateUrl: 'templates/navigation.html'
	};
});

app.directive('featuredGames', function(){	
	return {
		restrict: 'E', 
		templateUrl: 'templates/featuredGames.html'
	};
});